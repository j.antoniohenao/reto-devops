### Reto 1. Dockerize la aplicación
![docker](./img/nodedoker.jpg)

¿Qué pasa con los contenedores? En este momento **(2020)**, los contenedores son un estándar para implementar aplicaciones **(en la nube o en sistemas locales)**. Entonces, el reto es:
1. Construir la imagen más pequeña que pueda. Escribe un buen Dockerfile :)
2. Ejecutar la app como un usuario diferente de root.

**PASOS EJECUTADOS**

Tomar como Base para la creación de la imagen, el Dockerfile contenido en la ruta ./reto_jh/reto_1.  Para esta imagen, tomo como base la imagen de Centos:7, ya que genera una imagen mas pequeña que la imagen de node.


Para construir la imagen nos dirigimos a la carpeta reto_1 con el comando: 
`cd reto_jh/reto_1`
Y ejecutamos el siguiente comando para la creación de la imagen

`docker build -t node-image .`

Esto nos genera una imagen llamada node:latest, y ejecuta el aplicativo "node" contenido en este repositorio con un usuario llamado "nodeuserreto" según las instrucciones.

Luego ejecutar el siguiente comando para crear un contenedor en segundo plano y que nos mantenga la aplicación en ejecución:

`docker run -d --name node -p 3000:3000 node-image`

El comando anterior nos crea un contenedor el cual ejecuta una aplicacion node por el puerto 3000, y con nombre "node-app"

Para visualizar el contenedor en ejecución ejecutar el comando siguiente:

`docker ps`

Obtendremos una salida por terminal como esta:
```
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                    NAMES
fee6d19773e0        node                "/bin/sh -c 'node in…"   2 seconds ago       Up 1 second         0.0.0.0:3000->3000/tcp   node
```

### Reto 2. Docker Compose
![compose](./img/docker-compose.png)

Una vez que haya dockerizado todos los componentes de la API *(aplicación de NodeJs)*, estarás listo para crear un archivo docker-compose, en nuestro equipo solemos usarlo para levantar ambientes de desarrollo antes de empezar a escribir los pipeline. Ya que la aplicación se ejecuta sin ninguna capa para el manejo de protocolo http, añace:

1. Nginx que funcione como proxy reverso a nuesta app Nodejs
2. Asegurar el endpoint /private con auth_basic
3. Habilitar https y redireccionar todo el trafico 80 --> 443

**PASOS EJECUTADOS**

Para este segundo reto, se creó una imagen en un Dockerfile el cual se encuentra en la carpeta ./reto_jh/reto_2/Dockerfile

Para esta imagen se requieren 4 archivos:

```
nginx.conf -> /etc/nginx/
certs/ssl.crt -> /etc/pki/nginx/
certs/ssl.key -> /etc/pki/nginx/private/
.htpasswd -> /etc/nginx/
```

El archivo nginx.conf, es un archivo de configuración por defecto el cual fue modificado para este ejemplo. Dicho archivo presenta Nginx a través del puerto 80 y 443, con un certificado autofirmado para fines del test, con su respectiva llave.

Para obtener este certificado se utilizaron los siguientes comandos

`openssl req -new -nodes -keyout ssl.key -out ssl.csr -days 3650`

El comando anterior crea una solicitud de certificado (CSR) y una key (llave para el CSR), luego se genera un certificado autofirmado utilizando la key creada:

`openssl x509 -req -days 3650 -in ssl.csr -signkey ssl.key -out ssl.crt`

Luego se crea un archivo ".htpasswd", el cual contiene la contraseña de la autenticación básica solicitada para el endpoint /private.

`htpasswd -c .htpasswd test`


Las credenciales son las siguientes:
Usuario: test
Contraseña: Laclave123

Una vez creadas ambas imágenes, se procede a crear el archivo "docker-compose.yml". El contenido es el siguiente:

```
version: '3'
#Definimos los servicios a utilizar
services:
        nginx:
                container_name: nginx
                ports:
                   - "80:80"
                   - "443:443"
                image: nginx-image
        node: 
                container_name: node
                ports: 
                  - "3000:3000"
                image: node-image
```

Según la configuración del docker-compose, se lanzan dos contenedores: nginx y node. Basándose en las imágenes previamente creadas, reutilizando la imagen de "node" creada en el paso 1, donde el contenedor de nginx escucha por el puerto 80 y 443 y el contenedor de node escucha por el puerto 3000.

Por configuración de nginx, se configura que todo lo que llegue al puerto 80 sea redireccionado al puerto 443, el cual resuelve un proxy reverso que apunta al contenedor llamado "node" por el puerto 3000. Acá el extracto del archivo de configuración:

```
server {
        listen       443 ssl http2 default_server;
        server_name  _;
        root         /usr/share/nginx/html;

        ssl_certificate "/etc/pki/nginx/ssl.crt";
        ssl_certificate_key "/etc/pki/nginx/private/ssl.key";
     .
     .
     .
        location / {
        	proxy_pass http://node:3000;
        }
	location /private {
		proxy_pass http://node:3000/private;
		auth_basic "Acceso Restringido";
		auth_basic_user_file /etc/nginx/.htpasswd;
	}
```




### Reto 3. Probar la aplicación en cualquier sistema CI/CD
![cicd](./img/cicd.jpg)

Como buen ingeniero devops, conoces las ventajas de ejecutar tareas de forma automatizada. Hay algunos sistemas de cicd que pueden usarse para que esto suceda. Elige uno, travis-ci, gitlab-ci, circleci ... lo que quieras. Danos una tubería exitosa. **Gitlab-ci** es nuestra herramienta de uso diario por lo cual obtendras puntos extras si usas gitlab.

### Reto 4. Deploy en kubernetes
![k8s](./img/k8s.png)

Ya que eres una máquina creando contenedores, ahora queremos ver tu experiencia en k8s. Use un sistema kubernetes para implementar la API. Recomendamos que uses herramientas como minikube o microk8s.

Escriba el archivo de implementación (archivo yaml) utilizalo para implementar su API (aplicación Nodejs).

* añade **Horizontal Pod Autoscaler** a la app NodeJs

### Reto 5. Construir Chart en helm y manejar trafico http(s)
![helm](./img/helm-logo-1.jpg)

Realmente el pan de cada día es crear, modificar y usar charts de helm. Este reto consiste en:

1. Diseñar un chart de helm con nginx que funcione como proxy reverso a nuesta app Nodejs
2. Asegurar el endpoint /private con auth_basic
3. Habilitar https y redireccionar todo el trafico 80 --> 443

### Reto 6. Terraform
![docker](./img/tf.png)

En estos días en IaC no se habla de más nada que no sea terraform, en **CLM** ya nos encontramos con pipeline automatizados de Iac. El reto consiste en crear un modulo de terraform que nos permita crear un **rbac.authorization de tipo Role** que solo nos permita ver los pods de nuestro **namespace donde se encuentra al app Nodejs**

### Reto 7. Automatiza el despliegue de los retos realizados
![docker](./img/make.gif)

Ya que hoy en día no queremos recordar recetas ni comandos, el reto consiste en **automatizar los retos en un Makefile**, considera especificar cuales son las dependencias necesarias para que tu Makefile se ejecute sin problemas.

**NT:** Se evaluará el orden en el cual se encuentre el repositorio, en el gran universo de aplicaciones que existe hoy en día el orden es un factor importante.
